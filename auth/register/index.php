<?php
session_start();

$pdo = new PDO('mysql:host=localhost;dbname=spm_cms', 'root', '');
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tim Kempe">

    <meta name="generator" content="Jekyll v3.8.5">

    <title>spm-gruppe.de - Register</title>

    <!-- Bootstrap core CSS and JS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../assets/js/bootstrap.min.js"></script>


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

</head>

<body class="text-center">

<?php
$showform = true; //Variable ob das Registrierungsformular anezeigt werden soll

if (isset($_GET['register'])) {
    $error = false;
    $email = $_POST['email'];
    $username = $_POST['username'];
    $passwort = $_POST['passwort'];
    $passwort2 = $_POST['passwort2'];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo 'Bitte eine gültige E-Mail-Adresse eingeben<br>';
        $error = true;
    }
    if (strlen($passwort) == 0) {
        echo 'Bitte ein Passwort angeben<br>';
        $error = true;
    }
    if ($passwort != $passwort2) {
        echo 'Die Passwörter müssen übereinstimmen<br>';
        $error = true;
    }

    //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
    if(!$error) {
        $statement = $pdo->prepare("SELECTE * FROM users WHERE email = :email");
        $result = $statement-> execute(array('email' => $email));
        $user = $statement->fetch();

        if ($user !== false) {
            echo 'Diese E-Mail-Adresse ist bereits vergeben<br>';
            $error = true;
        }
    }

    //Keine Fehler, wir können den Nutzer registrieren
    if (!$error) {
        $passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);

        $statement = $pdo->prepare("INSERT INTO users (email, username, passwort) VALUES (:email, :username, :passwort)");
        $result = $statement->execute(array('email' => $email, 'username' => $username, 'passwort' => $passwort_hash));

        if ($result) {
            header('location: ../login/');
            $showform = false;
        } else {
            echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
        }
    }
}

if($showform) {
?>
<link href="register.css" rel="stylesheet">

<form action="?register=1" class="form-signin" method="post">

    <!-- header img -->
    <img class="mb-4" src="../../assets/img/logo/logo.png" alt="" width="72" height="72">

    <h1 class="h3 mb-3 font-weight-normal">Register - CMS</h1>

    <label for="inputEmail" class="sr-only">E-Mail</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-Mail" required>

    <br>

    <label for="inputUsername" class="sr-only">Benutzername</label>
    <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required>

    <br>

    <label for="inputPasswort" class="sr-only">Passwort</label>
    <input type="password" name="passwort" id="inputPasswort" class="form-control" placeholder="Passwort" required>

    <br>

    <label for="inputPasswort2" class="sr-only">Passwort wiederholen</label>
    <input type="password" name="passwort2" id="inputPasswort2" class="form-control" placeholder="Passwort wiederholen" required>

    <br>

    <button class="btn btn-lg btn-success btn-block" type="submit" value="Registrieren">Registrieren</button>

    <p class="mt-5 mb-3 text-muted">&copy; SPM Gruppe 2019</p>
</form>

<?php
} else {//end if($showform)
    ?>

    <p>Das Formular ist derzeit nicht Verfügbar.</p>

<?php
}
?>
</body>
</html>
