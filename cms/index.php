<?php
session_start();

if (!isset($_SESSION['username'])) {
    header('location: ../auth/login/');
}
$userid = $_SESSION['username'];

?>
<!DOCTYPE html>
<html lang="de">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>spm-gruppe.de - CMS</title>

  <!-- Custom fonts for this template-->
  <link href="./assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="./assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="./assets/css/sb-admin.css" rel="stylesheet">

  <script type="text/javascript">
    $(document).ready(function(){
      $('#t1').tooltip();
    });

    $(document).ready(function(){
      $('#t2').tooltip();
    });

    $(document).ready(function(){
      $('#t3').tooltip();
    });
  </script>

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.php">spm-gruppe.de</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- navbar - placeholder -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">

      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-envelope" data-toggle="tooltip" title="Nachrichten" id="t1"></i></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-user-cog" data-toggle="tooltip" title="Einstellungen" id="t2"></i></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="../auth/logout/"><i class="fas fa-sign-out-alt" data-toggle="tooltip" title="Logout" id="t3"></i> <?php echo $userid; ?></a>
      </li>

    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Admin</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Settings:</h6>
          <a class="dropdown-item" href="login.html">Login</a>
          <a class="dropdown-item" href="register.html">Register</a>
          <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item active" href="index.php">Blank Page</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="ticketdropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ticket-alt"></i>
                <span>Ticket-System</span>
            </a>

            <div class="dropdown-menu" aria-labelledby="ticketdropdown">
                <h6 class="dropdown-header">Admin:</h6>
                    <a class="dropdown-item" href="#">offene Tickets</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">User:</h6>
                    <a class="dropdown-item" href="#">Meine Tickets</a>
                    <a class="dropdown-item" href="#">Ticket erstellen</a>
            </div>
        </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.php">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Blank Page</li>
        </ol>

        <!-- Page Content -->
        <h1>Blank Page</h1>
        <hr>
        <p>This is a great starting point for new custom pages.</p>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>© spm-gruppe.de 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="./assets/vendor/jquery/jquery.min.js"></script>
  <script src="./assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="./assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="./assets/js/sb-admin.min.js"></script>

</body>

</html>
