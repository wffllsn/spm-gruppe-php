<?php
session_start();
$pdo = new PDO('mysql:host=localhost;dbname=spm_cms', 'root', '');

if(isset($_GET['login'])) {
    $username = $_POST['username'];
    $passwort = $_POST['passwort'];

    $statement = $pdo->prepare("SELECT * FROM users WHERE username = :username");
    $result = $statement->execute(array('username' => $username));
    $user = $statement->fetch();

    //check password
    if ($user !== false && password_verify($passwort, $user['passwort'])) {
        $_SESSION['username'] = $username;
        header('location: ../../cms/');
    } else {
        $errorMessage = '<div class="alert alert-danger">Benutzername oder Passwort sind ungültig!</div>';
    }

}
?>

<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Tim Kempe">

    <meta name="generator" content="Jekyll v3.8.5">

    <title>spm-gruppe.de - Login</title>

    <!-- Bootstrap core CSS and JS -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../assets/js/bootstrap.min.js"></script>


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
</head>

<body class="text-center">

<form action="?login=1" class="form-signin" method="post">

    <!-- header img -->
    <img class="mb-4" src="../../assets/img/logo/logo.png" alt="" width="72" height="72">

    <h1 class="h3 mb-3 font-weight-normal">Login - CMS</h1>

    <label for="inputUsername" class="sr-only">Benutzername</label>
    <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>

    <br>

    <label for="inputPasswort" class="sr-only">Passwort</label>
    <input type="password" name="passwort" id="inputPasswort" class="form-control" placeholder="Passwort" required>

    <br>

    <?php
    if (isset($errorMessage)) {
        echo $errorMessage;
    }
    ?>

    <!---
    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" value="remember-me"> Remember me
        </label>
    </div>
    -->

    <button class="btn btn-lg btn-success btn-block" type="submit" value="Login">Login</button>

    <p class="mt-5 mb-3 text-muted">&copy; SPM Gruppe 2019</p>
</form>

</body>
</html>
