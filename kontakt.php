<?php

require_once ("config/config.php");

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title><?php echo $titel ?></title>

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/bootstrap.min.js"></script>
</head>
<body>

<?php include ("templates/header.php"); ?>



<?php include ("templates/footer.php");?>

</body>
</html>